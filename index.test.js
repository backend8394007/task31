const functions = require('./index');

describe('sum', () => {
    it('2 + 2 = 4', async () => {
        const { assert } = await import('chai');
        assert.equal(functions.sum(2, 2), 4);
    });

    it ('5 + 5 = 10', async () =>{
       const {assert} = await import('chai');
       assert.equal(functions.sum(5, 5), 10);
    });

    it ('2.13 + 1.13 close to 3.3', async () =>{
        const {assert} = await import('chai');
        assert.closeTo(functions.sum(2.13, 1.13), 3.3, 1);
    });
});

describe('SumStr', () =>{
   it ('qwerty + 123', async () =>{
      const {assert} = await import('chai');
      assert.equal(functions.sumStr('qwerty', '123'), 'qwerty 123');
   });
});

describe('isNull', () =>{
   it ('3 % 2  = 1 (null)', async () =>{
      const {assert} = await import('chai');
      assert.equal(functions.isNull(3), null);
   });
    it ('4 % 2  = 0 (5)', async () =>{
        const {assert} = await import('chai');
        assert.equal(functions.isNull(4), 5);
    });
});

describe('subStr', () =>{
   it ('3 characters in the word "Hello"', async () =>{
      const {assert} = await import('chai');
      assert.equal(functions.mySubstr('Hello', 3), 'Hel');
   });
});



